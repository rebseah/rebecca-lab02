package com.example.mylab02;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.time.LocalTime;



@RestController
public class Rest {
    @GetMapping("/date")
    public String getDate() { return "Current date: " + LocalDate.now().toString(); }

    @GetMapping("/name")
    public String getName(){ return "Rebecca Seah";}

}
